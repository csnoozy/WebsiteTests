/**
 * Created by csnoo on 7/29/2016.
 */

var visCanvas = null;
var bufCanvas = null;
var visContext = null;
var bufContext = null;

var pointArray = [];
var pointTimer = null;
var maxPoints = 100;
var maxLength = 200;

function Point() {
    this.x = Math.round(Math.random() * visContext.canvas.width);
    this.y = Math.round(Math.random() * visContext.canvas.height);
    this.moveX = Math.random() > 0.5;
    this.moveY = Math.random() > 0.5;
    this.size = 1 + Math.round(Math.random() * 5);
    this.speed = .1 + Math.random();
    var shade = Math.round(Math.random()*100);
    this.color = 'rgb(' + shade + ', ' + shade + ', ' + shade + ')';
    //this.color = 'black';
}

function initArcSystems() {
    visCanvas = document.getElementById('myCanvas');
    if (visCanvas && visCanvas.getContext) {
        visContext = visCanvas.getContext('2d');
        visCanvas.width = getWidth();
        visCanvas.height = getHeight();


        bufCanvas = document.createElement("canvas");
        bufContext = bufCanvas.getContext('2d');
        bufContext.canvas.width = visContext.canvas.width;
        bufContext.canvas.height = visContext.canvas.height;

        pointTimer = setInterval(addPoint, 1);

        draw();

        setInterval(run, 10);
    }
}

function run() {
    update();
    draw();
}

function update() {
    for (var i = 0; i < pointArray.length; i++) {
        if (pointArray[i].moveX) {
            pointArray[i].x += pointArray[i].speed;
        } else {
            pointArray[i].x -= pointArray[i].speed;
        }
        if (pointArray[i].moveY) {
            pointArray[i].y += pointArray[i].speed;
        } else {
            pointArray[i].y -= pointArray[i].speed;
        }
        if (pointArray[i].x < 0 && pointArray[i].moveX == false) {
            pointArray[i].moveX = true;
        } else if (pointArray[i].x > bufContext.canvas.width && pointArray[i].moveX == true) {
            pointArray[i].moveX = false;
        }
        if (pointArray[i].y < 0 && pointArray[i].moveY == false) {
            pointArray[i].moveY = true;
        } else if (pointArray[i].y > bufContext.canvas.height && pointArray[i].moveY == true) {
            pointArray[i].moveY = false;
        }
    }
    bufContext.canvas.width = getWidth();
    bufContext.canvas.height = getHeight();
}

function addPoint() {
    pointArray[pointArray.length] = new Point();
    if (pointArray.length == maxPoints)
        clearInterval(pointTimer);
}

function blank() {
    bufContext.fillStyle = 'white';
    bufContext.fillRect(0, 0, bufContext.canvas.width, bufContext.canvas.height);
}

function draw() {
    visContext.save();

    blank();

    for (var one = 0; one < pointArray.length; one++) {
        for (var two = 0; two < pointArray.length; two++) {
            if (distance(pointArray[one].x, pointArray[one].y, pointArray[two].x, pointArray[two].y) < maxLength) {
                bounceLine(pointArray[one].x, pointArray[one].y, pointArray[two].x, pointArray[two].y);
            }
        }
    }

    for (var i = 0; i < pointArray.length; i++) {
        drawPoint(pointArray[i].x, pointArray[i].y, pointArray[i].size, pointArray[i].color);
    }

    visContext.drawImage(bufCanvas, 0, 0, bufCanvas.width, bufCanvas.height);
    visContext.restore();

}

function bounceLine(x1, y1, x2, y2) {
    bufContext.beginPath();
    bufContext.lineWidth = 1;
    bufContext.strokeStyle = getColor(distance(x1, y1, x2, y2));
    bufContext.moveTo(x1, y1);
    bufContext.lineTo(x2, y2);
    bufContext.stroke();
}

function drawPoint(x, y, size, color) {
    bufContext.beginPath();
    bufContext.lineWidth = 1;
    bufContext.strokeStyle = color;
    bufContext.fillStyle = color;
    bufContext.arc(x, y, size, 0, 360);
    bufContext.shadowColor = 'black';
    bufContext.shadowBlur = '3';
    bufContext.shadowOffsetX = 2;
    bufContext.shadowOffsetY = 2;
    bufContext.stroke();
    bufContext.fill();
}

function distance(x1, y1, x2, y2) {
    return Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
}

function getColor(dist) {
    var value = Math.ceil(55 + dist);
    return 'rgb(' + value + ', ' + value + ', ' + value + ')';
}

function getWidth() {
    if (self.innerHeight) {
        return self.innerWidth;
    }

    if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;
    }

    if (document.body) {
        return document.body.clientWidth;
    }
}

function getHeight() {
    if (self.innerHeight) {
        return self.innerHeight;
    }

    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }

    if (document.body) {
        return document.body.clientHeight;
    }
}