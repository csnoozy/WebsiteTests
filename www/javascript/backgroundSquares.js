/**
 * Created by Fire on 7/31/2016.
 */

var backgroundColor = '#404040';
var grid = [];
var gridSpace = 50;
var c = null;
var ctx = null;
var bc = null;
var bctx = null;
var shade = true;

function Square(x, y) {
    this.size = getRandomInt(gridSpace / 1.5, gridSpace - 5);
    this.r = getRandomInt(0, 0);
    this.g = getRandomInt(0, 50);
    this.b = getRandomInt(0, 255);
    this.s = getRandomInt(-45, 75);
    this.fillColor = function () {
        return 'rgb(' + this.r + ', ' + this.g + ', ' + this.b + ')';
    };
    this.fillShade = function () {
        return 'rgb(' + Math.round(this.s + this.size*2) + ', ' + Math.round(this.s + this.size*2) + ', ' + Math.round(this.s + this.size*2) + ')';
    };
    this.shadowColor = 'black';
    this.x = x;
    this.y = y;
    this.inc = Math.random() > .5;
    this.speed = getRandomArbitrary(.1, .2);
}

function initBackgroundSquares() {
    c = document.getElementById("myCanvas");
    if (c && c.getContext) {
        ctx = c.getContext('2d');
        c.width = getWidth();
        c.height = getHeight();

        bc = document.createElement('canvas');
        bctx = bc.getContext('2d');
        bc.width = c.width;
        bc.height = c.height;

        for (var x = 0; x < c.width * 2; x += gridSpace) {
            for (var y = 0; y < c.height * 2; y += gridSpace) {
                if (Math.random() > .7) {
                    grid[grid.length] = new Square(x, y);
                }
            }
        }

        setInterval(runBG, 60);
    }
}

function runBG() {

    updateBG();
    drawBG();
}

function updateBG() {
    c.width = getWidth();
    c.height = getHeight();
    bc.width = getWidth();
    bc.height = getHeight();

    for (var i = 0; i < grid.length; i++) {
        if (grid[i].size < (gridSpace - 5) && grid[i].inc) {
            grid[i].size += grid[i].speed;
        } else if (grid[i].size > (gridSpace / 1.5) && !grid[i].inc) {
            grid[i].size -= grid[i].speed;
        } else if (grid[i].size > (gridSpace - 5) && grid[i].inc) {
            grid[i].inc = false;
        } else if (grid[i].size < (gridSpace / 1.5) && !grid[i].inc) {
            grid[i].inc = true;
        }
    }

}

function blank() {
    bctx.fillStyle = backgroundColor;
    bctx.fillRect(0, 0, bctx.canvas.width, bctx.canvas.height);
}

function drawBG() {
    ctx.save();
    blank();

    for (var i = 0; i < grid.length; i++) {
        drawSquare(grid[i]);
    }

    ctx.drawImage(bc, 0, 0, bctx.canvas.width, bctx.canvas.height);
    ctx.restore();
}

function drawSquare(square) {
    if (!shade)
        bctx.fillStyle = square.fillColor();
    else
        bctx.fillStyle = square.fillShade();
    bctx.shadowColor = square.shadowColor;
    bctx.shadowBlur = 2;
    bctx.shadowOffsetX = (gridSpace - 15) - square.size;
    bctx.shadowOffsetY = (gridSpace - 15) - square.size;
    bctx.fillRect(square.x, square.y, square.size, square.size);
}


// Helper Functions
function getWidth() {
    if (self.innerHeight) {
        return self.innerWidth;
    }

    if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;
    }

    if (document.body) {
        return document.body.clientWidth;
    }
}

function getHeight() {
    if (self.innerHeight) {
        return self.innerHeight;
    }

    if (document.documentElement && document.documentElement.clientHeight) {
        return document.documentElement.clientHeight;
    }

    if (document.body) {
        return document.body.clientHeight;
    }
}

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}