/*
 * Reclamation: Reclaim the Galaxy.
 */

var gameSave = {
    lunarCredits: 100,

    timePlayed: 1,
    lastTime: null,

    fuel: {
        value: 50,
        perSecond: 0,
        total: 50
    },

    farmers: {
        value: 0,
        perSecond: 0,
        total: 0,
        produce: 1,
        cost: 10
    },

    tractors: {
        value: 0,
        perSecond: 0,
        total: 0,
        produce: 1,
        cost: 10
    },

    g3: 0,
    g4: 0,
    g5: 0,
    g6: 0,
    g7: 0,
    g8: 0,
    g9: 0,
    g10: 0
};

function driver() {
    restore();
    createDisplay();
    draw();
    setInterval(loop, 1000);
}

function createDisplay() {
    document.getElementById('version').innerHTML = version;
    document.getElementById('date').innerHTML = Date();
}

function loop() {
    update();
    draw();
    save();
}

function save() {
    gameSave.lastTime = new Date();
    localStorage.setItem('studioSave', JSON.stringify(gameSave));
    if (doConsole)
        console.log('Data Saved: ' + JSON.stringify(gameSave));
}

function restore() {
    if (localStorage.getItem('studioSave')) {
        gameSave = JSON.parse(localStorage.getItem('studioSave'));
        if (doConsole)
            console.log('Save Restored: ' + JSON.stringify(gameSave));

        // Account for passed time in values!!!
    }
    localStorage.clear();
}

function changeTab(event, tabTitle) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("contentPane");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tabLink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activeTab", "");
    }
    document.getElementById(tabTitle).style.display = "block";
    event.currentTarget.className += " activeTab";
}

function changeSidebar(event, tabTitle) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("contentInfo");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("sidebarLink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" activeList", "");
    }
    document.getElementById(tabTitle).style.display = "block";
    event.currentTarget.className += " activeList";
}

var version = 'v0.0.58';
var lunarCreditsCode = '&#x2104;';
var doConsole = false;
var tipReady = true;


function update() {
    var farmerChange = gameSave.tractors.value *gameSave.tractors.produce;
    gameSave.farmers.value += farmerChange;
    gameSave.farmers.total += farmerChange;
    var fuelChange = gameSave.farmers.value * gameSave.farmers.produce;
    gameSave.fuel.value += fuelChange;
    gameSave.fuel.total += fuelChange;

    gameSave.timePlayed++;
}

function hireGatherer(gatherer, amount) {
    switch (gatherer) {
        case 'farmers':
            if (gameSave.fuel.value >= gameSave.farmers.cost * amount) {
                gameSave.fuel.value -= gameSave.farmers.cost * amount;
                gameSave.farmers.value += amount;
                gameSave.farmers.total += amount;
            }
            break;
        case 'tractors':
            if (gameSave.farmers.value >= gameSave.tractors.cost * amount) {
                gameSave.farmers.value -= gameSave.tractors.cost * amount;
                gameSave.tractors.value += amount;
                gameSave.tractors.total += amount;
            }
            break;
    }
    draw();
}

function draw() {
    if (gameSave.farmers.value > 1000)
        document.getElementById('tractorsList').style.display = 'block';

    gameSave.farmers.perSecond = gameSave.tractors.value;
    gameSave.fuel.perSecond = gameSave.farmers.value;

    dHelperC('fuelValue', gameSave.fuel.value);
    dHelperC('fuelPerSecond', gameSave.fuel.perSecond);
    dHelperC('fuelCollected', gameSave.fuel.total);

    dHelperC('farmers', gameSave.farmers.value);
    dHelperC('farmersPerSecond', gameSave.farmers.perSecond);
    dHelperC('farmersHired', gameSave.farmers.total);
    dHelperC('farmersProduce', gameSave.farmers.produce);

    dHelperC('tractors', gameSave.tractors.value);
    dHelperC('tractorsPerSecond', gameSave.tractors.perSecond);
    dHelperC('tractorsHired', gameSave.tractors.total);
    dHelperC('tractorsProduce', gameSave.tractors.produce);


    if (tipReady)
        document.getElementById('tip').style.display = 'block';
    else
        document.getElementById('tip').style.display = 'none';
    document.getElementById('date').innerHTML = Date();

    document.getElementById('title').innerHTML = 'Project Reclamation ' + gameSave.timePlayed;
}

function dHelperC(c, value) {
    var elements = document.getElementsByClassName(c);
    for (var i = 0; i < elements.length; i++) {
        elements[i].innerHTML = value;
    }
}